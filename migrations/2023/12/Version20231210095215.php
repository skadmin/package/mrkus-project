<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231210095215 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE mrkus_project (id INT AUTO_INCREMENT NOT NULL, is_system TINYINT(1) DEFAULT 0 NOT NULL, created_at DATETIME NOT NULL, last_update_at DATETIME DEFAULT NULL, is_active TINYINT(1) DEFAULT 1 NOT NULL, webalize VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, code VARCHAR(255) NOT NULL, date_of_publishing DATETIME DEFAULT NULL, image_preview VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');

        $translations = [
            ['original' => 'mrkus-project.overview', 'hash' => '02ba39ceb3f771625fc50fc61373ab5b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Projekty', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mrkus-project.overview.title', 'hash' => '67f360b3189afa9522ae57822631768f', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Projekty|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.mrkus-project.overview.action.new', 'hash' => 'db7c93652dd1afe51cb1c7492dd02175', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založit projekt', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.mrkus-project.overview.name', 'hash' => 'fe743222a70dcfaaad91ee1667c7c71c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.mrkus-project.overview.title', 'hash' => 'ae7256f6b19657161c846dde07d12882', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Titulek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.mrkus-project.overview.is-active', 'hash' => '0ab465c69eaa48c18231af2ffe57ec4b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní?', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mrkus-project.edit.title', 'hash' => 'd4788261b974f1dff0bdb864afc80d92', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Založení projektu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.name', 'hash' => 'a7fe4a33a32e9282982746fecd98fb8e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.name.req', 'hash' => '3ff85fc63a26897b63311858682c885e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zadejte prosím název', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.title', 'hash' => '55749aea287bfcafef941c9b64aa7932', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Titulek', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.date-of-publishing', 'hash' => 'd5169e9034aeb29c9a1391d0ce40d22e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Datum publikace', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.content', 'hash' => '7ffab01649963a46efe454ac7defd139', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Obsah', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.is-active', 'hash' => '5021e2791239db9e34b983f6f0c12c4a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Je aktivní', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.send', 'hash' => 'b82fbc8470ac763aa6bae0f313c3feb2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.send-back', 'hash' => 'dc94346144cd587ece6e6c5ded017c46', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Uložit a zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.back', 'hash' => '35044a0868c1ae09cfc57dc5090ca712', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zpět', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.flash.success.create', 'hash' => '2dce27d65f427525e3ecc54f4fa5edb2', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Projekt byl úspěšně založen.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.mrkus-project.overview.action.edit', 'hash' => 'd70522ac297ba62d699810f21651c5d9', 'module' => 'admin', 'language_id' => 1, 'singular' => '', 'plural1' => '', 'plural2' => ''],
            ['original' => 'mrkus-project.edit.title - %s', 'hash' => '5eaf161fd543048b8847563c23b3c3b3', 'module' => 'admin', 'language_id' => 1, 'singular' => '%s|Editace projektu', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.change-webalize', 'hash' => '008b5f38c8bdb9db14c33a2fd6045675', 'module' => 'admin', 'language_id' => 1, 'singular' => 'přegenerovat url', 'plural1' => '', 'plural2' => ''],
            ['original' => 'form.mrkus-project.edit.flash.success.update', 'hash' => 'd217959efcf105d33fc16d39385a06cb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Projekt byl úspěšně upraven.', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE mrkus_project');
    }
}
