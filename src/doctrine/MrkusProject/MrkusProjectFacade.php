<?php

declare(strict_types=1);

namespace Skadmin\MrkusProject\Doctrine\MrkusProject;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Nettrine\ORM\EntityManagerDecorator;
use SkadminUtils\DoctrineTraits\Facade;
use DateTimeInterface;

use function assert;

final class MrkusProjectFacade extends Facade
{
    use Facade\Webalize;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct($em);
        $this->table = MrkusProject::class;
    }

    public function create(string $name, string $title, string $content, bool $isActive, DateTimeInterface $dateOfPublishing, ?string $imagePreview): MrkusProject
    {
        return $this->update(null, $name, $title, false, $content, $isActive, $dateOfPublishing, $imagePreview);
    }

    public function update(?int $id, string $name, string $title, bool $changeWebalize, string $content, bool $isActive, DateTimeInterface $dateOfPublishing, ?string $imagePreview): MrkusProject
    {
        $mrkusProject = $this->get($id);
        $mrkusProject->update($name, $title, $content, $isActive, $dateOfPublishing, $imagePreview);

        if (!$mrkusProject->isLoaded()) {
            $mrkusProject->setWebalize($this->getValidWebalize($name));
        } elseif ($changeWebalize) {
            $mrkusProject->updateWebalize($this->getValidWebalize($name));
        }

        $this->em->persist($mrkusProject);
        $this->em->flush();

        return $mrkusProject;
    }

    public function get(?int $id = null): MrkusProject
    {
        if ($id === null) {
            return new MrkusProject();
        }

        $user = parent::get($id);

        if ($user === null) {
            return new MrkusProject();
        }

        return $user;
    }

    public function findByWebalize(string $webalize): ?MrkusProject
    {
        $criteria = ['webalize' => $webalize];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function findByCode(string $code): ?MrkusProject
    {
        $criteria = ['code' => $code];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    /**
     * @return MrkusProject[]
     */
    public function findForSitemap(): array
    {
        return $this->getAll(true);
    }

    /**
     * @return MrkusProject[]
     */
    public function getAll(bool $onlyActive = false, ?bool $isSystem = null): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = true;
        }

        if ($isSystem !== null) {
            $criteria['isSystem'] = $isSystem;
        }

        $orderBy = ['name' => 'ASC'];

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria, $orderBy);
    }

    public function findByDate(?int $limit = null, ?int $offset = null, bool $includeSystem = false): array
    {
        $qb = $this->em->createQueryBuilder();

        $qb->select('a')
            ->from($this->table, 'a')
            ->where('a.isActive = :isActive')
            ->setParameter('isActive', true)
            ->orderBy('a.isSystem', 'DESC')
            ->addOrderBy('a.dateOfPublishing', 'DESC')
            ->addOrderBy('a.id', 'DESC')
            ->setFirstResult($offset)
            ->setMaxResults($limit);

        if (!$includeSystem) {
            $qb->andWhere('a.isSystem = :isSystem')
                ->setParameter('isSystem', false);
        }

        return $qb->getQuery()->getResult();
    }
}
