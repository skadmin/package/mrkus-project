<?php

declare(strict_types=1);

namespace Skadmin\MrkusProject\Doctrine\MrkusProject;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;
use DateTimeInterface;

use function boolval;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class MrkusProject
{
    use Entity\BaseEntity;
    use Entity\IsActive;
    use Entity\WebalizeName;
    use Entity\Title;
    use Entity\Content;
    use Entity\Code;
    use Entity\DateOfPublishing;
    use Entity\ImagePreview;

    #[ORM\Column(options: ['default' => false])]
    private bool $isSystem = false;

    public function update(string $name, string $title, string $content, bool $isActive, DateTimeInterface $dateOfPublishing, ?string $imagePreview): void
    {
        $this->name = $name;
        $this->title = $title;
        $this->content = $content;
        $this->dateOfPublishing = $dateOfPublishing;

        $this->setIsActive($isActive);

        if ($imagePreview !== null && $imagePreview !== '') {
            $this->imagePreview = $imagePreview;
        }
    }

    public function isSystem(): bool
    {
        return $this->isSystem;
    }

}
