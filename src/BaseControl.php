<?php

declare(strict_types=1);

namespace Skadmin\MrkusProject;

use App\Model\System\ABaseControl;
use Nette\Utils\ArrayHash;
use Nette\Utils\Html;

class BaseControl extends ABaseControl
{
    public const RESOURCE = 'mrkus-project';
    public const DIR_IMAGE = 'mrkus-project';

    public function getMenu(): ArrayHash
    {
        return ArrayHash::from([
            'control' => $this,
            'icon'    => Html::el('i', ['class' => 'fas fa-fw fa-drafting-compass']),
            'items'   => ['overview'],
        ]);
    }
}
