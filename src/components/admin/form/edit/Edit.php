<?php

declare(strict_types=1);

namespace Skadmin\MrkusProject\Components\Admin;

use App\Model\Doctrine\User\User;
use App\Model\System\APackageControl;
use App\Model\System\Flash;
use Nette\ComponentModel\IContainer;
use Nette\Security\User as LoggedUser;
use Nette\Utils\ArrayHash;
use Nette\Utils\DateTime;
use Nette\Utils\Html;
use Skadmin\MrkusProject\BaseControl;
use Skadmin\MrkusProject\Doctrine\MrkusProject\MrkusProject;
use Skadmin\MrkusProject\Doctrine\MrkusProject\MrkusProjectFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\SimpleTranslation;
use Skadmin\Translator\Translator;
use SkadminUtils\FormControls\UI\Form;
use SkadminUtils\FormControls\UI\FormWithUserControl;
use SkadminUtils\FormControls\Utils\UtilsFormControl;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;
use SkadminUtils\ImageStorage\ImageStorage;

class Edit extends FormWithUserControl
{
    use APackageControl;

    private LoaderFactory $webLoader;
    private MrkusProjectFacade $facade;
    private User $user;
    private MrkusProject $project;
    private ImageStorage $imageStorage;


    public function __construct(?int $id, MrkusProjectFacade $facade, Translator $translator, LoaderFactory $webLoader, LoggedUser $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;
        $this->webLoader = $webLoader;
        $this->user = $this->loggedUser->getIdentity(); //@phpstan-ignore-line
        $this->imageStorage = $imageStorage;

        $this->project = $this->facade->get($id);
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function getTitle(): SimpleTranslation|string
    {
        if ($this->project->isLoaded()) {
            return new SimpleTranslation('mrkus-project.edit.title - %s', $this->project->getName());
        }

        return 'mrkus-project.edit.title';
    }

    /**
     * @return CssLoader[]
     */
    public function getCss(): array
    {
        return [
            $this->webLoader->createCssLoader('daterangePicker'),
            $this->webLoader->createCssLoader('customFileInput'),
            $this->webLoader->createCssLoader('fancyBox'), // responsive file manager
        ];
    }

    /**
     * @return JavaScriptLoader[]
     */
    public function getJs(): array
    {
        return [
            $this->webLoader->createJavaScriptLoader('adminTinyMce'),
            $this->webLoader->createJavaScriptLoader('moment'),
            $this->webLoader->createJavaScriptLoader('daterangePicker'),
            $this->webLoader->createJavaScriptLoader('customFileInput'),
            $this->webLoader->createJavaScriptLoader('fancyBox'), // responsive file manager
        ];
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/edit.latte');

        $template->project = $this->project;
        $template->render();
    }

    protected function createComponentForm(): Form
    {
        $form = new Form();
        $form->setTranslator($this->translator);

        // INPUT
        $form->addText('name', 'form.mrkus-project.edit.name')
            ->setRequired('form.mrkus-project.edit.name.req');
        $form->addText('title', 'form.mrkus-project.edit.title');
        $form->addTextArea('content', 'form.mrkus-project.edit.content');
        $form->addCheckbox('isActive', 'form.mrkus-project.edit.is-active')
            ->setDefaultValue(true);
        $form->addText('dateOfPublishing', 'form.mrkus-project.edit.date-of-publishing')
            ->setHtmlAttribute('data-date');

        $form->addImageWithRFM('imagePreview', 'form.news.edit.image-preview');

        // EXTEND
        if ($this->project->isLoaded()) {
            $form->addCheckbox('changeWebalize', Html::el('sup', [
                'title' => $this->translator->translate('form.mrkus-project.edit.change-webalize'),
                'class' => 'far fa-fw fa-question-circle',
                'data-toggle' => 'tooltip',
                'data-placement' => 'left',
            ]))->setTranslator(null);
        }

        // BUTTON
        $form->addSubmit('send', 'form.mrkus-project.edit.send');
        $form->addSubmit('sendBack', 'form.mrkus-project.edit.send-back');
        $form->addSubmit('back', 'form.mrkus-project.edit.back')
            ->setValidationScope([])
            ->onClick[] = [$this, 'processOnBack'];

        // DEFAULT
        $form->setDefaults($this->getDefaults());

        // CALLBACK
        $form->onSuccess[] = [$this, 'processOnSuccess'];

        return $form;
    }

    /**
     * @return mixed[]
     */
    private function getDefaults(): array
    {
        if (!$this->project->isLoaded()) {
            return [];
        }

        return [
            'name' => $this->project->getName(),
            'title' => $this->project->getTitle(),
            'content' => $this->project->getContent(),
            'isActive' => $this->project->isActive(),
            'dateOfPublishing' => $this->project->getDateOfPublishing()->format('d.m.Y'),
        ];
    }

    public function processOnSuccess(Form $form, ArrayHash $values): void
    {
        $dateOfPublishing = DateTime::createFromFormat('d.m.Y', $values->dateOfPublishing);

        // IDENTIFIER
        $identifier = UtilsFormControl::getImagePreview($values->imagePreview, BaseControl::DIR_IMAGE);

        if ($this->project->isLoaded()) {
            if ($identifier !== null && $this->project->getImagePreview() !== null) {
                $this->imageStorage->delete($this->project->getImagePreview());
            }

            $project = $this->facade->update($this->project->getId(), $values->name, $values->title, $values->changeWebalize, $values->content, $values->isActive, $dateOfPublishing, $identifier);
            $this->onFlashmessage('form.mrkus-project.edit.flash.success.update', Flash::SUCCESS);
        } else {
            $project = $this->facade->create($values->name, $values->title, $values->content, $values->isActive, $dateOfPublishing, $identifier);
            $this->onFlashmessage('form.mrkus-project.edit.flash.success.create', Flash::SUCCESS);
        }

        if ($form->isSubmitted()->name === 'sendBack') {
            $this->processOnBack();
        }

        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'edit',
            'id' => $project->getId(),
        ]);
    }

    public function processOnBack(): void
    {
        $this->getPresenter()->redirect('Component:default', [
            'package' => new BaseControl(),
            'render' => 'overview',
        ]);
    }
}
