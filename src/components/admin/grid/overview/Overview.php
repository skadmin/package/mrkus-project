<?php

declare(strict_types=1);

namespace Skadmin\MrkusProject\Components\Admin;

use App\Model\Grid\Traits\IsActive;
use App\Model\System\APackageControl;
use App\Model\System\Callback;
use App\Model\System\Constant;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Arrays;
use Nette\Utils\Html;
use Skadmin\MrkusProject\BaseControl;
use Skadmin\MrkusProject\Doctrine\MrkusProject\MrkusProject;
use Skadmin\MrkusProject\Doctrine\MrkusProject\MrkusProjectFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;
use SkadminUtils\ImageStorage\ImageStorage;

use function is_array;
use function method_exists;

class Overview extends GridControl
{
    use APackageControl;
    use IsActive;

    private MrkusProjectFacade $facade;
    private ImageStorage  $imageStorage;

    public function __construct(MrkusProjectFacade $facade, Translator $translator, User $user, ImageStorage $imageStorage)
    {
        parent::__construct($translator, $user);

        $this->facade = $facade;
        $this->imageStorage = $imageStorage;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (!$this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'mrkus-project.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.isSystem', 'DESC')
            ->addOrderBy('a.dateOfPublishing', 'DESC')
            ->addOrderBy('a.id', 'DESC')
        );

        // DATA
        $translator = $this->translator;
        $dialYesNo = Arrays::map(Constant::DIAL_YES_NO, static function ($text) use ($translator): string {
            return $translator->translate($text);
        });

        // COLUMNS
        $grid->addColumnText('imagePreview', '')
            ->setRenderer(function (MrkusProject $mp): ?Html {
                if ($mp->getImagePreview() !== null) {
                    $imageSrc = $this->imageStorage->fromIdentifier([$mp->getImagePreview(), '60x60', 'exact']);

                    return Html::el('img', [
                        'src'   => sprintf('/%s', $imageSrc->createLink()),
                        'style' => 'max-width: none;',
                    ]);
                }

                return null;
            })->setAlign('center');

        $grid->addColumnText('name', 'grid.mrkus-project.overview.name')
            ->setRenderer(function (MrkusProject $mp): Html {
                if ($this->isAllowed(BaseControl::RESOURCE, Privilege::WRITE)) {
                    $link = $this->getPresenter()->link('Component:default', [
                        'package' => new BaseControl(),
                        'render' => 'edit',
                        'id' => $mp->getId(),
                    ]);

                    $name = Html::el('a', [
                        'href' => $link,
                        'class' => 'font-weight-bold',
                    ]);
                } else {
                    $name = new Html();
                }

                $name->setText($mp->getName());

                return $name;
            });
        $grid->addColumnText('title', 'grid.mrkus-project.overview.title');
        $this->addColumnIsActive($grid, 'mrkus-project.overview');

        // STYLE
        $grid->getColumn('imagePreview')
            ->getElementPrototype('th')
            ->setAttribute('style', 'width: 1px');

        // FILTER
        $grid->addFilterText('name', 'grid.mrkus-project.overview.name');
        $grid->addFilterText('title', 'grid.mrkus-project.overview.title');
        $this->addFilterIsActive($grid, 'mrkus-project.overview');
        $grid->addFilterText('lastUpdateAuthor', 'grid.mrkus-project.overview.lastUpdateAuthor');

        // ACTION
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addAction('edit', 'grid.mrkus-project.overview.action.edit', 'Component:default', ['id' => 'id'])->addParameters([
                'package' => new BaseControl(),
                'render' => 'edit',
            ])->setIcon('pencil-alt')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // TOOLBAR
        if ($this->isAllowed(BaseControl::RESOURCE, 'write')) {
            $grid->addToolbarButton('Component:default', 'grid.mrkus-project.overview.action.new', [
                'package' => new BaseControl(),
                'render' => 'edit',
            ])->setIcon('plus')
                ->setClass('btn btn-xs btn-default btn-primary');
        }

        // IF USER ALLOWED WRITE

        // ALLOW

        return $grid;
    }
}
