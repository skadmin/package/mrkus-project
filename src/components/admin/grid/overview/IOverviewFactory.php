<?php

declare(strict_types=1);

namespace Skadmin\MrkusProject\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
